﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyDamageDisplay : MonoBehaviour
{
  public void DestroyDisplay()
  {
    Destroy(gameObject);
  }
}
