using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndGame : MonoBehaviour
{
    [SerializeField] private Image _fadeImage;
    
    private DialogueManager _dialogueManager;
    private Player _player;
    private PlayerUI _playerUI;
    
   [SerializeField] private GameObject  _conversationWindow;
   [SerializeField] private GameObject _needSoulsText;
   [SerializeField] private GameObject _goHomeText;
   [SerializeField] private GameObject _rButtonText;

   private bool _soulsCollected = false;
   private bool _monologue = false;
   
    void Start()
    {
        _dialogueManager = FindObjectOfType<DialogueManager>();
        _player = FindObjectOfType<Player>();
        _playerUI = FindObjectOfType<PlayerUI>();
    }
    
    void Update()
    {
        if (_fadeImage.color.a >= 1 && _soulsCollected)
        {
            _rButtonText.SetActive(true);
            _conversationWindow.SetActive(false);
            _goHomeText.SetActive(false);
            _soulsCollected = false;
            SceneManager.LoadScene("End");
        }

        if (Input.GetKeyDown(KeyCode.R) && _monologue)
        {
            _playerUI.EndGameButton.SetActive(false);
            _conversationWindow.SetActive(false);
            _player.enabled = true;
            _monologue = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _playerUI.EndGameButton.SetActive(true);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                _playerUI.EndGameButton.SetActive(false);
                
                if (_dialogueManager.Soulcounter >= 3)
                {
                    _rButtonText.SetActive(false);
                    _conversationWindow.SetActive(true);
                    _goHomeText.SetActive(true);
                    _soulsCollected = true;
                    _fadeImage.DOFade(1, 5);
                    _player.enabled = false;
                    _player.PlayerAnimation.PlayerSpeed(0);
                }
                
                else if (_dialogueManager.Soulcounter < 3)
                {
                    _conversationWindow.SetActive(true);
                    _needSoulsText.SetActive(true);
                    _monologue = true;
                    _player.enabled = false;
                    _player.PlayerAnimation.PlayerSpeed(0);
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _playerUI.EndGameButton.SetActive(false);
            _conversationWindow.SetActive(false);
            _needSoulsText.SetActive(false);
        }
        
    }
}
