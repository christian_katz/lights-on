﻿using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndScreen : MonoBehaviour
{
    [SerializeField] private Image _fadeImage;

    // Start is called before the first frame update
    void Start()
    {
        MasterAudio.StopAllOfSound("Forest");
        MasterAudio.StopAllOfSound("Wind");
        MasterAudio.StopAllOfSound("TreeRustle");
        MasterAudio.ChangePlaylistByName("PlaylistController","Start");
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        

            _fadeImage.DOFade(0, 3);
    }
    
    public void BackToMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
