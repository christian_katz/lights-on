﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyClone : MonoBehaviour
{
    private bool _cloneActiv = false;
    private Enemy _enemy;
    private ParticleSystem[] _effects;
    void Start()
    {
        _effects = GetComponentsInChildren<ParticleSystem>();
        
        for (int i = 0; i < _effects.Length; i++)
        {
            _effects[i].Stop();
        }
        _cloneActiv = true;
        _enemy = FindObjectOfType<Enemy>();
    }
    
    void Update()
    {
        CloneDuration();
    }
    
    public void CloneDuration()
    {
        if (_cloneActiv)
        {
            _enemy.CloneCooldown -= Time.deltaTime;
            
            if ( _enemy.CloneCooldown <= 0)
            {
                _enemy.CloneCooldown = 2;
                _cloneActiv = false;
                Destroy(gameObject);
            }
        }
    }
}
