﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using DarkTonic.MasterAudio;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Enemy : MonoBehaviour
{
    private IEnemyState _currentState;
    public static EnemyRepositionState EnemyRepositionState = new EnemyRepositionState();
    public static EnemyAttackState EnemyAttackState = new EnemyAttackState();
    public static EnemyRunState EnemyRunState = new EnemyRunState();
    public static EnemyChargeAttack EnemyChargeAttack = new EnemyChargeAttack();
    public static EnemyRangeAttackState EnemyRangeAttackState = new EnemyRangeAttackState();

    private EnemyAnimationController _enemyAnimationController;

    public EnemyAnimationController EnemyAnimationController
    {
        get => _enemyAnimationController;
        set => _enemyAnimationController = value;
    }

    [Header("Stats")]
    [SerializeField] private float _attackrange;
    [SerializeField] private float _attacksToReposition;
    [SerializeField] private float _repositionRangeMultiplyer;

    private float _acceleration;
    private float _movementSpeed;
    private bool _activateDamage;
    private float _damageCooldown = 0.1f;
    private bool _rotateToPlayer;
    private float _distance;
    private float _numberOfConsecutiveAttacks;
    public float NumberOfConsecutiveAttacks => _numberOfConsecutiveAttacks;
    
    public float AttacksToReposition => _attacksToReposition;

    public float RepositionRangeMultiplyer => _repositionRangeMultiplyer;

    public float AttackRange => _attackrange;

    private NavMeshAgent _agent;
    public NavMeshAgent Agent => _agent;

    private Player _player;
    public Player Player => _player;
    
    private FightZone _currentFightZone;
    
    [Header("Health Bar")]
    [SerializeField] private float _health;
    
    [Header("Damage Display")]
    [SerializeField] private Transform _spawnDamagePosition;
    [SerializeField] private GameObject _damageDisplay;
    [SerializeField] private ParticleSystem _getHitEffect;
    
    [Header("Sword")]
    [SerializeField] private GameObject _enemySword;

    [Header("Range Attack Settings")]
    [SerializeField] private float _rangeAttackCooldown = 5;
    [SerializeField] private GameObject _magicBall;
    [SerializeField] private Transform _shootPosition;
    private float _rangeCooldown;
    private bool _rangeAttack = false;
    public bool RangeAttack1
    {
        get => _rangeAttack;
        set => _rangeAttack = value;
    }
    
    [Header("Charge Attack Settings")]
    [SerializeField] private Transform _chargeAttackRayPosition;
    [SerializeField] private float _chargeAttacks;
    [SerializeField] private float _chargeAttackCooldown = 5;
    [SerializeField] private float _aimingPlayerSpeed = 5;
    [SerializeField] private float _chargeSpeed = 1000;
    [SerializeField] private float _chargeAcceleration = 200;
    [SerializeField] private ParticleSystem[] _chargeEffect;
    [SerializeField] private Collider _chargeAttackDamageCollider;
    private bool _chargeAttack = false;
    private bool _charge = false;
    private float _chargeCooldown;
    private float _chargeCounter;
    private bool _updatePoint = true;

    private float _pointDistance = 0;
    private float _findCooldown = 0.2f;

    [Header("Teleportation Skill")]
    [SerializeField] private Material _cloneMaterial;
    [SerializeField] private float _cloneDuration;
    [SerializeField] private GameObject _teleportationEffectPrefab;
    [SerializeField] private GameObject _teleportationEffetPosition;

    public float CloneCooldown
    {
        get => _cloneDuration;
        set => _cloneDuration = value;
    }
    
    public bool ChargeAttack1
    {
        get => _chargeAttack;
        set => _chargeAttack = value;
    }
    
    public bool Charge
    {
        get => _charge;
        set => _charge = value;
    }
    
    private float _farestDistance;
    private Transform _attackPoint;
    private int _chooseAttackPoint = 0;
    private bool _firstMeleeAttack = false;
    
    // stop the permanent attack animation when get in attack range
    private bool _stopAttackAnimation = false;

    public bool StopAttackAnimation
    {
        get => _stopAttackAnimation;
        set => _stopAttackAnimation = value;
    }

    void Start()
    {
        _player = FindObjectOfType<Player>();
        _agent = GetComponent<NavMeshAgent>();
        _currentState = EnemyRunState;
        _agent.stoppingDistance = _attackrange;
        _enemyAnimationController = GetComponent<EnemyAnimationController>();

        _agent.SetDestination(_player.transform.position);

        _movementSpeed = _agent.speed;
        _acceleration = _agent.acceleration;
        _rangeCooldown = _rangeAttackCooldown;
        _chargeCooldown = _chargeAttackCooldown;
        
        for (int i = 0; i < _chargeEffect.Length; i++)
        {
            _chargeEffect[i].Stop();
        }
    }

    void Update()
    {
        IEnemyState nextState = _currentState.Execute(this);
        if (nextState != _currentState)
        {
            _currentState.Exit(this);
            _currentState = nextState;
            _currentState.Enter(this);
        }
        
        RotateToPlayer();
        
        DeactivateSwordCooldown();
        ChargeAttackCountdown();
        RangeAttackCountdown();
        
        Debug.DrawRay(_chargeAttackRayPosition.position, transform.forward * 100, Color.blue);
    }

    public bool InRange()
    {
        _agent.SetDestination(_player.transform.position);
        //return Vector3.Distance(transform.position, _player.transform.position) < _attackrange;
        return _agent.remainingDistance < _agent.stoppingDistance;
    }

    public void RunTowardsPlayer()
    {
        _agent.isStopped = false;
        _agent.SetDestination(_player.transform.position);
        _enemyAnimationController.RunAnimation();
    }
    
    /// <summary>
    /// When entering melee Attack state and at the end of the attack animation, this method will be called with an animation event
    /// </summary>
    public void AttackPlayer()
    {
        if (!_stopAttackAnimation)
        {
            if (!_firstMeleeAttack)
            {
                _enemyAnimationController.MeleeAttackAnimation();
                _firstMeleeAttack = true;
            }

            // check in which melee attack animation the player is and it will activate the next one
            _enemyAnimationController.MeleeAttackCheck();
        
            _numberOfConsecutiveAttacks += 1;
            _rotateToPlayer = true;
        }

    }

    public void ResetAtHeavyAttack()
    {
        _firstMeleeAttack = false;
    }
    
    /// <summary>
    /// when going back to run state, this method will reset the following variables
    /// </summary>
    public void ResetAttackCounter()
    {
        _numberOfConsecutiveAttacks = 0;
        _firstMeleeAttack = false;
        _stopAttackAnimation = true;
    }

    public void CurrentFightZone(FightZone fightZone)
    {
        _currentFightZone = fightZone;
    }

    /// <summary>
    /// should rotate to the player after the attack animation or at the start of the attack
    /// </summary>
    public void RotateToPlayer()
    {
        if (_rotateToPlayer)
        {
            Vector3 lookToPlayer = (_player.transform.position - transform.position).normalized;
            transform.rotation = Quaternion.Slerp(transform.rotation,Quaternion.LookRotation(lookToPlayer), Time.deltaTime * 5);

            if (Vector3.Dot( transform.forward, (_player.transform.position - transform.position).normalized) >= 0.90f)
            {
                _rotateToPlayer = false;
            }
        }
    }

    public void GetDamage()
    {
        //_enemyAnimationController.GetHitAnimation();
        _health -= Player.DamageValue;
        _currentFightZone.EnemyHealthBar.fillAmount -= Player.DamageValue / 150;
        Quaternion _damageDisplayDirection = Quaternion.LookRotation(-(Camera.main.transform.position - transform.position).normalized);

        Instantiate(_damageDisplay, _spawnDamagePosition.position, _damageDisplayDirection);
        _getHitEffect.Play();

        if (_health <= 0)
        {
            MasterAudio.PlaySound("Alien Death_01");
            _currentFightZone.EnemyDead();
            _currentFightZone.EnemyHealthBar.transform.parent.gameObject.SetActive(false);
            Player.OnDamage -= GetDamage;
            Destroy(gameObject);
        }
    }

    /// <summary>
    /// Animation Event
    /// </summary>
    public void ActivateSwordDamage()
    {
        _enemySword.GetComponent<Collider>().enabled = true;
        _activateDamage = true;
    }

    private void DeactivateSwordCooldown()
    {
        if (_activateDamage)
        {
            _damageCooldown -= Time.deltaTime;
            if (_damageCooldown <= 0)
            {
                _enemySword.GetComponent<Collider>().enabled = false;
                _damageCooldown = 0.1f;
                _activateDamage = false;
            }
        }
    }

    public void SpawnHologramClone()
    {
        GameObject clone = Instantiate(gameObject, transform.position, Quaternion.identity);

        clone.GetComponent<NavMeshAgent>().enabled = false;
        clone.GetComponent<Collider>().enabled = false;
        clone.GetComponent<Animator>().enabled = false;
        clone.GetComponent<Enemy>().enabled = false;

        clone.AddComponent<DestroyClone>();
        
        SkinnedMeshRenderer[] skinnedMeshList = clone.GetComponentsInChildren<SkinnedMeshRenderer>();
        clone.transform.Find("rig/root/DEF-spine/DEF-spine.001/DEF-spine.002/DEF-spine.003/Wing.002").GetComponent<MeshRenderer>().material = _cloneMaterial;
        clone.transform.Find("rig/root/DEF-spine/DEF-spine.001/DEF-spine.002/DEF-spine.003/WingFrame.003").GetComponent<MeshRenderer>().material = _cloneMaterial;
        clone.transform.Find("rig/root/DEF-spine/DEF-spine.001/DEF-spine.002/DEF-spine.003/DEF-spine.004/DEF-spine.005/DEF-spine.006/Horns").GetComponent<MeshRenderer>().material = _cloneMaterial;
        clone.transform.Find("Eyes.006").gameObject.SetActive(false);
        

        for (int i = 0; i < skinnedMeshList.Length; i++)
        {
            skinnedMeshList[i].material = _cloneMaterial;
        }

    }
    
    public void SpawnTeleportationEffect()
    {
        Instantiate(_teleportationEffectPrefab, _teleportationEffetPosition.transform.position, Quaternion.identity);
        SpawnHologramClone();
    }
    
    public void ChooseFarestPosition()
    {
        float distanceToPlayer;

        for (int i = 0; i < _currentFightZone.RangeAttackPoints.Length; i++)
        {
            distanceToPlayer = Vector3.Distance(_currentFightZone.RangeAttackPoints[i].transform.position, _player.transform.position);
            
            if (distanceToPlayer > _farestDistance)
            {
                _attackPoint = _currentFightZone.RangeAttackPoints[i].transform;
                _farestDistance = distanceToPlayer;
            }
        }

        MasterAudio.PlaySound("TeleportationSound");
        Vector3 lookToPlayer = (_player.transform.position - transform.position).normalized;  
        transform.rotation = Quaternion.LookRotation(lookToPlayer);

        SpawnTeleportationEffect();
        transform.position = _attackPoint.position;

        if (_rangeAttack)
        {
            _enemyAnimationController.IdleAnimation();
            _enemyAnimationController.RangeAttackAnimation();
        }
        
        if(_chargeAttack)
        {
            _charge = true;
            _chargeAttackDamageCollider.enabled = true;
            
            for (int i = 0; i < _chargeEffect.Length; i++)
            {
                _chargeEffect[i].Play();
            }
           
        }
    }

    public void ShootMagicBall()
    {
        Vector3 lookToPlayer = (_player.transform.position - transform.position).normalized;  
        transform.rotation = Quaternion.LookRotation(lookToPlayer);
        
       Instantiate(_magicBall, _shootPosition.position, Quaternion.LookRotation((_player.transform.position - transform.position).normalized));

       MasterAudio.PlaySound("ShootMagicBall");
       
       // after 3 different attack spots the enemy goes back to another action
       if (_chooseAttackPoint >= 2)
       {
           _rangeAttack = false;
           _chooseAttackPoint = 0;
           return;
       }
       
       // with that i get the second and third farest point
       _chooseAttackPoint++;
       _farestDistance = 0;
       ChooseFarestPosition();
    }

    public void RangeAttackCountdown()
    {
        _rangeCooldown -= Time.deltaTime;

        if (_rangeCooldown <= 0)
        {
            _rangeAttack = true;
            _rangeCooldown = 30;            
        }
    }
    
    public void ChargeAttackCountdown()
    {
        _chargeCooldown -= Time.deltaTime;

        if (_chargeCooldown <= 0)
        {
            _chargeAttack = true;
            _chargeCooldown = 60; 
        }
    }

    public void ChargeAttack()
    {
        // calculate *2 because the charge counter counts +2 every charge instead of 1, thus we have the desired charge number
        if (_chargeCounter >= _chargeAttacks * 2)
        {
            for (int i = 0; i < _chargeEffect.Length; i++)
            {
                _chargeEffect[i].Stop();
            }
            _chargeAttack = false;
            _chargeCounter = 0;
            _agent.speed = _movementSpeed;
            _agent.acceleration = _acceleration;

            _chargeAttackDamageCollider.enabled = false;
            //transform.position = _player.transform.position;
            return;
        }
        
        if (_charge)
        {
            RaycastHit hit;
            if (Physics.Raycast(_chargeAttackRayPosition.position, transform.forward, out hit,100, LayerMask.GetMask("Wall")))
            {
                _agent.speed = _chargeSpeed;
                _agent.acceleration = _chargeAcceleration;
                _agent.SetDestination(hit.point);
                _agent.isStopped = false;

                if (_updatePoint)
                {
                    _pointDistance = _agent.remainingDistance;
                    _updatePoint = false;
                }

                _findCooldown -= Time.deltaTime;

                if (_findCooldown <= 0)
                {
                    if (_agent.remainingDistance == _pointDistance)
                    {
                        _updatePoint = true;
                        _findCooldown = 0.2f;
                        ChooseFarestPosition();
                    }
                    else
                    {
                        _findCooldown = 0.2f;
                        _updatePoint = true;
                    }
                }
            }
            
            if (_agent.remainingDistance <= 3f)
            {
                _charge = false;
                _chargeCounter++;
                _agent.isStopped = true;
                Debug.Log("I am there");
            }
            Debug.Log("charge!");
            Debug.Log(_agent.remainingDistance);
        }
        else if(!_charge)
        {
            Vector3 lookToPlayer = (_player.transform.position - transform.position).normalized;
            transform.rotation = Quaternion.Slerp(transform.rotation,Quaternion.LookRotation(lookToPlayer), Time.deltaTime * _aimingPlayerSpeed);
            
            if (Vector3.Dot( transform.forward, (_player.transform.position - transform.position).normalized) >= 0.99f)
            {
                _charge = true;
                _updatePoint = false;
                _enemyAnimationController.RunAnimation();
                _agent.isStopped = false;
                MasterAudio.PlaySound("ChargeAttack");
                MasterAudio.PlaySound("Alien Effect_01");
            }
        }
    }

    public void EnemyDisappear()
    {
        _enemyAnimationController.IdleAnimation();
        _enemyAnimationController.enabled = false;
        FindObjectOfType<EnemyDissolveControl>().Dissolve = true;
        enabled = false;

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Sword"))
        {
            GetDamage();
        }
    }
}
