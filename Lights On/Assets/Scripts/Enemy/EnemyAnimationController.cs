﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimationController : MonoBehaviour
{
    private Animator _animator;

    public Animator Animator => _animator;
    private ImpactReceiver _impactReceiver;
    private static readonly int Run = Animator.StringToHash("run");
    private static readonly int Idle = Animator.StringToHash("idle");
    private static readonly int MeleeAttack = Animator.StringToHash("meleeAttack");
    private static readonly int MeleeAttack2 = Animator.StringToHash("meleeAttack2");
    private static readonly int RangeAttack = Animator.StringToHash("rangeAttack");
    private static readonly int MeleeHeavyAttack = Animator.StringToHash("meleeHeavyAttack");
    private static readonly int Block = Animator.StringToHash("block");
    private static readonly int getHit = Animator.StringToHash("getHit");

    private Enemy _enemy;

    void Start()
    {
        _animator = GetComponent<Animator>();
        _impactReceiver = GetComponent<ImpactReceiver>();
        _enemy = GetComponent<Enemy>();
    }
    
    public void MeleeAttackCheck()
    {
        if (_animator.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
        {
            _animator.ResetTrigger(MeleeAttack);
            _animator.SetTrigger(MeleeAttack2);
        }
        else if (_animator.GetCurrentAnimatorStateInfo(0).IsName("Attack2"))
        {
            _animator.ResetTrigger(MeleeAttack2);
            _animator.SetTrigger(MeleeHeavyAttack);
            
        }
        else if (_animator.GetCurrentAnimatorStateInfo(0).IsName("Attack3"))
        {
            // With the reset, the enemy can attack with the first melee Attack
            _enemy.ResetAtHeavyAttack();
            _animator.ResetTrigger(MeleeHeavyAttack);
        }
    }

    /// <summary>
    /// With that the the range Attack animation can't be played without goes back into the melee Attack
    /// </summary>
    public void ResetMeleeAttackTrigger()
    {
        _animator.ResetTrigger(MeleeAttack);
        _animator.ResetTrigger(MeleeAttack2);
        _animator.ResetTrigger(MeleeHeavyAttack);
    }
    
    public void IdleAnimation()
    {
        _animator.SetTrigger(Idle);
    }

    public void ResetIdleAnimation()
    {
        _animator.ResetTrigger(Idle);
    }
    
    public void RunAnimation()
    {
        _animator.SetTrigger(Run);
    }

    public void ResetRunAnimation()
    {
        _animator.ResetTrigger(Run);
    }

    public void MeleeAttackAnimation()
    {
        _animator.SetTrigger(MeleeAttack);
    }
    
    public void MeleeAttackAnimation2()
    {
        _animator.SetTrigger(MeleeAttack2);
    }

    public void MeleeHeavyAttackAnimation()
    {
        _animator.SetTrigger(MeleeHeavyAttack);
    }

    public void RangeAttackAnimation()
    {
        _animator.SetTrigger(RangeAttack);
    }

    public void BlockAnimation()
    {
        _animator.SetTrigger(Block);
    }

    public void GetHitAnimation()
    {
        _animator.SetTrigger(getHit);
    }
    
}
