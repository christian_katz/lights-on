﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using DarkTonic.MasterAudio;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine.UI;

public class FightZone : MonoBehaviour
{
    [Header("Setup")]
    [SerializeField] private string _playertag;
    [SerializeField] private Transform[] _spawnPoints;
    [SerializeField] private GameObject _enemyPrefab;
    [SerializeField] private GameObject[] _wayBlockades;
    [SerializeField] private Transform[] _rangeAttackPoints;

    [SerializeField] private Image _enemyHealthBar;

    [SerializeField] private HealEnvironment _healEnvironment;


    private SpawnSouls _spawnSouls;

    public GameObject[] WayBlockades
    {
        get => _wayBlockades;
        set => _wayBlockades = value;
    }

    private Volume _volume;
    
    public Image EnemyHealthBar
    {
        get => _enemyHealthBar;
        set => _enemyHealthBar = value;
    }

    public Transform[] RangeAttackPoints
    {
        get => _rangeAttackPoints;
        set => _rangeAttackPoints = value;
    }
    
    private FightZone[] _fightzones;
    private float _enemyCount;
    private bool _fight = true;

    private GameObject _enemy;

    public GameObject Enemy => _enemy;

    private Player _player;

    private bool _fadeVignette = false;
    private bool _fadeIn = true;

    void Start()
    {
        _volume = FindObjectOfType<Volume>();
        _fightzones = FindObjectsOfType<FightZone>();
        _player = FindObjectOfType<Player>();
    }

    private void Update()
    {
        FadeVignette();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(_playertag) && _fight)
        {
            _fadeVignette = true;
            _fadeIn = true;
            MasterAudio.StopAllOfSound("Forest");
            
            for (int i = 0; i < _spawnPoints.Length; i++)
            {
                _player.CurrentFightZone(this);
               _enemy = Instantiate(_enemyPrefab, _spawnPoints[i].position, Quaternion.identity);
               _enemy.GetComponent<Enemy>().CurrentFightZone(this);
               _enemyHealthBar.transform.parent.gameObject.gameObject.SetActive(true);
               _enemyHealthBar.fillAmount = 1;
            }

            for (int i = 0; i < _wayBlockades.Length; i++)
            {
                _wayBlockades[i].SetActive(true);
                MasterAudio.PlaySound("StartFightEnemy");
            }
            
            _enemyCount = _spawnPoints.Length;
            _fight = false;
        }
    }

    public void EnemyDead()
    {
        _fadeIn = false;
        MasterAudio.PlaySound("Forest");
        _fadeVignette = true;
        _healEnvironment.enabled = true;
        
        _enemyCount--;

        if (_enemyCount <= 0)
        {
            for (int i = 0; i < _fightzones.Length; i++)
            {
                _fightzones[i].gameObject.SetActive(true);
            }
            
            MasterAudio.ChangePlaylistByName("PlaylistController","Playing");
            GetComponent<Collider>().enabled = false;
        }
    }

    private void FadeVignette()
    {
        if (_fadeVignette)
        {
            _volume.profile.TryGet<Vignette>(out var vignette);
            
            if (_fadeIn)
            {
                vignette.smoothness.value += Time.deltaTime * 0.5f;
                vignette.intensity.value = 0.33f;
            }
             if(!_fadeIn)
            {
                vignette.smoothness.value -= Time.deltaTime * 0.5f;
                vignette.intensity.value = 0.3f;
            }

            if( _fadeIn && vignette.smoothness.value >= 1)
            {
                _fadeVignette = false;
                _fadeIn = false;
            }
            if(!_fadeIn && vignette.smoothness.value < 0.4f)
            {
                _fadeVignette = false;
                _fadeIn = true;
                gameObject.SetActive(false);
            }
        }
    }
}
