﻿using System;
using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;
using UnityEngine;

public class ShootMagicBall : MonoBehaviour
{
    private Player _player;
    private Rigidbody _rigidbody;
    private Vector3 _flyingDirection;
    
    private void Start()
    {
        _player = FindObjectOfType<Player>();
        _rigidbody = GetComponent<Rigidbody>();
        _flyingDirection = (_player.transform.position - transform.position).normalized;
        _flyingDirection.y += 0.1f;
    }

    void Update()
    {
        _rigidbody.AddForce(_flyingDirection * (Time.deltaTime * 300), ForceMode.Impulse);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") || other.CompareTag("Ground"))
        {
            MasterAudio.PlaySound("MagicBallImpact");
            Destroy(gameObject);
            
        }
    }
}
