﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttackState : IEnemyState
{
    private float _attackStartedTime;
    
    public IEnemyState Execute(Enemy enemy)
    {
        bool repositioning = enemy.NumberOfConsecutiveAttacks > enemy.AttacksToReposition;
        if (repositioning)
        {
            return Enemy.EnemyRepositionState;
        }
        
        if (!enemy.InRange())
        {
            return Enemy.EnemyRunState;
        }
        
        return Enemy.EnemyAttackState;
    }

    public void Enter(Enemy enemy)
    {
        enemy.StopAttackAnimation = false;
        enemy.AttackPlayer();
    }

    public void Exit(Enemy enemy)
    {
        enemy.ResetAttackCounter();
        enemy.Agent.isStopped = false;
        enemy.EnemyAnimationController.ResetMeleeAttackTrigger();
    }
}

