﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyChargeAttack : IEnemyState
{
    public IEnemyState Execute(Enemy enemy)
    {
        Debug.Log("state");
        
        if (!enemy.ChargeAttack1)
        {
            return Enemy.EnemyRunState;
        }
        
        enemy.ChargeAttack();
        return Enemy.EnemyChargeAttack;
    }

    public void Enter(Enemy enemy)
    {
        enemy.ChooseFarestPosition();
    }

    public void Exit(Enemy enemy)
    {
        enemy.Charge = false;
    }
}
