﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRangeAttackState : IEnemyState
{
    public IEnemyState Execute(Enemy enemy)
    {
        if (!enemy.RangeAttack1)
        {
            return Enemy.EnemyRunState;
        }
        
        return Enemy.EnemyRangeAttackState;
    }

    public void Enter(Enemy enemy)
    {
        enemy.Agent.isStopped = true;
        enemy.ChooseFarestPosition();
    }

    public void Exit(Enemy enemy)
    {
        enemy.EnemyAnimationController.ResetIdleAnimation();
        enemy.Agent.isStopped = false;
    }
}
