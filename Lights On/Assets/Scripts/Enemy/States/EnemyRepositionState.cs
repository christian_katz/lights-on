﻿using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;
using UnityEngine;

public class EnemyRepositionState : IEnemyState
{
    private Vector3 _nextOffsetToPlayer;

    public IEnemyState Execute(Enemy enemy)
    {
        if (enemy.Agent.remainingDistance > enemy.AttackRange)
        {
            enemy.Agent.SetDestination(enemy.Player.transform.position + _nextOffsetToPlayer);
        }
        else
        {
            return Enemy.EnemyAttackState;
        }

        return  Enemy.EnemyRepositionState;
    }

    public void Enter(Enemy enemy)
    {
        enemy.SpawnTeleportationEffect();
        MasterAudio.PlaySound("TeleportationSound");
        _nextOffsetToPlayer = Random.insideUnitSphere * (enemy.AttackRange * enemy.RepositionRangeMultiplyer);
        enemy.transform.position = enemy.Player.transform.position + _nextOffsetToPlayer;
    }

    public void Exit(Enemy enemy)
    {
        
    }
}
