﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRunState : IEnemyState
{
    public IEnemyState Execute(Enemy enemy)
    {
        if (!enemy.InRange())
        {
            if (enemy.RangeAttack1)
            {
                return Enemy.EnemyRangeAttackState;
            }

            if (enemy.ChargeAttack1)
            {
                return Enemy.EnemyChargeAttack;
            }
            
            enemy.RunTowardsPlayer();
            return Enemy.EnemyRunState;
        }
        else
        {
            return Enemy.EnemyAttackState;
        }
    }

    public void Enter(Enemy enemy)
    {

    }

    public void Exit(Enemy enemy)
    {
        enemy.EnemyAnimationController.ResetRunAnimation();
    }
}
