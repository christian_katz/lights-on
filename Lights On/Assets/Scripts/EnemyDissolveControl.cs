﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDissolveControl : MonoBehaviour
{
   [SerializeField] private Material[] _dissolveMaterial;

    private float _fadeOutTime = 0;

     private float _fadeTime = 1;
    
    public float FadeTime
    {
        get => _fadeTime;
        set => _fadeTime = value;
    }

    [SerializeField] private float _fadeInSpeed = 0;


    private bool _goVisible = true;

    public bool GOVisible
    {
        get => _goVisible;
        set => _goVisible = value;
    }

    // the order to go invisible or not
    private bool _goInvisible = false;

    // activates the dissolve cooldown
    private bool _dissolve = true;

    public bool Dissolve
    {
        get => _dissolve;
        set => _dissolve = value;
    }

    private float _cooldown;

    public float Cooldown
    {
        get => _cooldown;
        set => _cooldown = value;
    }

    public bool GOInvisible
    {
        get => _goInvisible;
        set => _goInvisible = value;
    }

    void Start()
    {
        _goVisible = true;
    }

    void Update()
    {
        ObjectAppearing();
    }

    private void ObjectAppearing()
    {
        if (_dissolve)
        {
            // object will be visible
            if (_goVisible)
            {
                _fadeTime -= Time.deltaTime * _fadeInSpeed / 10;

                for (int i = 0; i < _dissolveMaterial.Length; i++)
                {
                    _dissolveMaterial[i].SetFloat("Vector1_9C8F1CA7", _fadeTime);
                    if (_dissolveMaterial[i].GetFloat("Vector1_9C8F1CA7") <= 0)
                    {
                        _dissolveMaterial[i].SetFloat("Vector1_9C8F1CA7", 0);
                        _goVisible = false;
                        _dissolve = false;
   
                    }
                }
            }

            // object will be invisible
            if (!_goVisible)
            {
                _fadeTime += Time.deltaTime * _fadeInSpeed / 10;

                for (int i = 0; i < _dissolveMaterial.Length; i++)
                {
                    _dissolveMaterial[i].SetFloat("Vector1_9C8F1CA7", _fadeTime);

                    if (_dissolveMaterial[i].GetFloat("Vector1_9C8F1CA7") >= 1)
                    {
                        _goVisible = true;
                        _dissolve = false;
                        Destroy(gameObject);
                    }
                }
            }
        }
        else
        {
            for (int i = 0; i < _dissolveMaterial.Length; i++)
            {
                _dissolveMaterial[i].SetFloat("Vector1_9C8F1CA7", 0);
            }
            
        }
        
       
    }
}
