﻿using System;
using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;
using UnityEngine;

public class KeyStone : MonoBehaviour
{
    [SerializeField] private string _playertag;
    [SerializeField] private GameObject _key;
    private PlayerUI _playerUI;

    private OpenGate _openGate;

    void Start()
    {
        _playerUI = FindObjectOfType<PlayerUI>();
        _openGate = FindObjectOfType<OpenGate>();
        _key.SetActive(false);
    }
    
    
    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag(_playertag))
        {
            _playerUI.EndGameButton.SetActive(true);
            if (Input.GetKeyDown(KeyCode.F) && _openGate.CollectedKeys > 0)
            {
                _key.SetActive(true);
                GetComponent<Collider>().enabled = false;
                _playerUI.EndGameButton.SetActive(false);
                _openGate.CollectedKeys--; 
                _openGate.KeyPlaced();
                MasterAudio.PlaySound("Picklock Safe (Success) 3");
            }
        }
    }
    
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(_playertag))
        {
            _playerUI.EndGameButton.SetActive(false);
        }
    }
}
