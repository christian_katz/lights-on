using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class ChangeEmission : MonoBehaviour
{
    private Renderer _renderer;
    private Color _color;
    private Color _startColor;

    private bool _enemyWarning = false;
    
    public bool EnemyWarning
    {
        get => _enemyWarning;
        set => _enemyWarning = value;
    }

    // the seconds how long the color is changing --> 2 sec is 1 color change
    private float _warningTimer;
    
    void Start()
    {
        _renderer = GetComponent<Renderer>();
        _startColor = _renderer.material.color;
    }
    
    void Update()
    {
        if (_enemyWarning)
        {
            float t = Mathf.PingPong(Time.time, 1);
            _color = Color.Lerp(_startColor, Color.red, t);
            _renderer.material.SetColor("_EmissionColor", _color);

            _warningTimer += Time.deltaTime;
            
            if (_warningTimer > 4)
            {
                _enemyWarning = false;
                _warningTimer = 0;
            }
        }

        if (_color != _startColor && !_enemyWarning)
        {
            _color = Color.Lerp(_renderer.material.color, _startColor, Time.deltaTime * 10);
            _renderer.material.SetColor("_EmissionColor", _color);
        }
    }
}
