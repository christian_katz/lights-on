using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeactivateWarningText : MonoBehaviour
{
    [SerializeField] private MonologueTrigger[] _monologueTriggers;
    
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _monologueTriggers[0].gameObject.SetActive(false);
            _monologueTriggers[1].gameObject.SetActive(false);
            _monologueTriggers[2].gameObject.SetActive(false);
            gameObject.SetActive(false);
            Debug.Log("yes");
        }
    }
}
