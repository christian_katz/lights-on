using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Dialogue
{
    public string NpcName;
    public string CharacterName;
    
    [TextArea(5,20)]
    public string[] Sentences;
    
}
