using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _npcName;
    [SerializeField] private TextMeshProUGUI _characterName;
    public TextMeshProUGUI DialogueText;
    private Player _player;
    private int _switchNames;

    [SerializeField] private GameObject _conversationObjects;
    [SerializeField] private GameObject _playerHUD;
    
    private Queue<string> sentences = new Queue<string>();
    
    private bool _endConversation = false;

    private int _soulcounter;

    public int Soulcounter
    {
        get => _soulcounter;
        set => _soulcounter = value;
    }

    public bool EndConversation
    {
        get => _endConversation;
        set => _endConversation = value;
    }
    
    void Start()
    {
        _player = FindObjectOfType<Player>();
        
    }
    
    public void StartDialogue(Dialogue dialogue)
    {
        _endConversation = false;
        _conversationObjects.SetActive(true);
        _playerHUD.SetActive(false);
        
        _player.enabled = false;
        
        _player.PlayerAnimation.PlayerSpeed(0);
        _npcName.text = dialogue.NpcName;
            _characterName.text = dialogue.CharacterName;
        
            sentences.Clear();

            foreach (string sentence in dialogue.Sentences)
            {
                sentences.Enqueue(sentence);
            }
            
        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {
        if (sentences.Count == 0)
        {
            EndDialogue();
            return;
        }

        if (_switchNames == 0)
        {
            _npcName.gameObject.SetActive(true);
            _characterName.gameObject.SetActive(false);
            
            _switchNames = 1;
        }
        else if (_switchNames == 1)
        {
            _npcName.gameObject.SetActive(false);
            _characterName.gameObject.SetActive(true);
            
            _switchNames = 0;
        }


        string sentence = sentences.Dequeue();
        DialogueText.text = sentence;
    }

    private void EndDialogue()
    {
        _playerHUD.SetActive(true);
        _player.enabled = true;
        _conversationObjects.SetActive(false);
        _endConversation = true;
        _switchNames = 0;

        _soulcounter++;
    }
}
