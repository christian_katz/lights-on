using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManagerStartScene : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _characterName;
    public TextMeshProUGUI DialogueText;
    
    [SerializeField] private GameObject _conversationObjects;
    
    private Queue<string> sentences = new Queue<string>();

    private StartScene _startScene;

    private void Start()
    {
        _startScene = GetComponent<StartScene>();
    }

    public void StartDialogue(Dialogue dialogue)
    {
        _characterName.text = dialogue.CharacterName;
        
        sentences.Clear();

        foreach (string sentence in dialogue.Sentences)
        {
            sentences.Enqueue(sentence);
        }
            
        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {
        if (sentences.Count == 0)
        {
            EndDialogue();
            return;
        }
        
        string sentence = sentences.Dequeue();
        DialogueText.text = sentence;
    }

    private void EndDialogue()
    {
        _conversationObjects.SetActive(false);
        _startScene.FadeImage.DOFade(1, 3);
    }
}
