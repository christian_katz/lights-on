using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTriggerStartScene : MonoBehaviour
{
    public Dialogue Dialogue;

    public void TriggerDialogue()
    {
        FindObjectOfType<DialogueManagerStartScene>().StartDialogue(Dialogue);
    }
    
}
