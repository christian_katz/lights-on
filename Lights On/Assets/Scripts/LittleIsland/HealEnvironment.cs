using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class HealEnvironment : MonoBehaviour
{
    [SerializeField] private GameObject _environment;
    [SerializeField] private ParticleSystem[] _healingParticle;
    [SerializeField] private Image _fadeImage;

    [SerializeField] private FightZone _fightZone;
    [SerializeField] private Transform _soulSpawnPoint;
    
    [SerializeField] private GameObject _healing;
    [SerializeField] private GameObject _healingTrigger;
    
    private SpawnSouls _spawnSouls;
    
    private Player _player;
    private bool _fadeIn = true;

    private int counter = 0;
    
    void Start()
    {
        _player = FindObjectOfType<Player>();
        _spawnSouls = FindObjectOfType<SpawnSouls>();
        _healingTrigger.SetActive(true);
    }

    private void Update()
    {
        if (_fadeImage.color.a >= 0.99f && _fadeIn)
        {
            _player.PlayerAnimation.Heal(false);
            
            _spawnSouls.CurrentSpawnLocation = _soulSpawnPoint;
            _spawnSouls.SpawnSoul();
            
            _environment.SetActive(true);
            _healingParticle[0].gameObject.SetActive(false);
            _healingParticle[1].gameObject.SetActive(false);
            _fadeIn = false;
            _fightZone.WayBlockades[0].SetActive(false);
            _fadeImage.DOFade(0, 3);
            
            _player.enabled = true;
            _healing.SetActive(false);
            _healingTrigger.SetActive(false);
        }
        
        if (_fadeImage.color.a <= 0 && !_fadeIn)
        {
            // set the script offline so that there will be no conflict with the soul spawn
            enabled = false;
        }
        
    }

    public void ChangeEnvironment()
    {
        _player.PlayerAnimation.Heal(true);
        _healing.SetActive(true);
        _healingParticle[0].gameObject.SetActive(true);
        _healingParticle[1].gameObject.SetActive(true);
        _fadeImage.DOFade(1, 7);
        _player.enabled = false;
        _player.PlayerAnimation.PlayerSpeed(0);
        _fadeIn = true;
    }
}
