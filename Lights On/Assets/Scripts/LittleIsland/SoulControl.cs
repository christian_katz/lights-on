using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.AI;

public class SoulControl : MonoBehaviour
{
    private NavMeshAgent _navMeshAgent;

    private PlayerUI _playerUI;
    private DialogueTrigger _dialogueTrigger;
    private DialogueManager _dialogueManager;

    [SerializeField] private Transform _destination;

    private bool _interacted = false;

    private bool _moveToPoint = false;

    [SerializeField] private Transform _soul;

    private bool _soulConversation = false;

    private bool _talk = false;
    
    void Start()
    {
        _playerUI = FindObjectOfType<PlayerUI>();
        _dialogueTrigger = GetComponent<DialogueTrigger>();
        _dialogueManager = FindObjectOfType<DialogueManager>();

        _dialogueManager.EndConversation = false;
        _playerUI.TalkKey.SetActive(false);
    }

    private void Update()
    {
        if (_dialogueManager.EndConversation && _soulConversation)
        {
            _soul.position = Vector3.MoveTowards(_soul.position, _destination.position, Time.deltaTime * 10);
        }
        
        _soul.position = new Vector3(_soul.position.x, _soul.position.y + Mathf.Sin(Time.time) * 0.01f, _soul.position.z);

        if (_talk)
        {
            if (Input.GetKeyDown(KeyCode.F) && !_interacted)
            {
                _dialogueTrigger.TriggerDialogue();
                
                _playerUI.TalkKey.SetActive(false);
                _interacted = true;
                _soulConversation = true;
            }
            
            if (Input.GetKeyDown(KeyCode.R))
            {
                _dialogueManager.DisplayNextSentence();
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !_interacted)
        {
            _playerUI.TalkKey.SetActive(true);
        }
        
    }
    
    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _talk = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _playerUI.TalkKey.SetActive(false);
            _talk = false;
        }
    }
}
