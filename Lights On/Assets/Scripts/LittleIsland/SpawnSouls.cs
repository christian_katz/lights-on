using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnSouls : MonoBehaviour
{
    // get the current span location of the fightzone
    private Transform _currentSpawnLocation;

    private float _countdown = 10;
    
    private void Update()
    {
        _countdown -= Time.deltaTime;

        if (_countdown <= 0)
        {
           
            _countdown = 10;
        }
    }

    public Transform CurrentSpawnLocation
    {
        get => _currentSpawnLocation;
        set => _currentSpawnLocation = value;
    }

   [SerializeField] private GameObject[] _souls;

    private bool _firstSoul = false;
    private bool _secondSoul = false;
    private bool _thirdSoul = false;
    
    public void SpawnSoul()
    {
        if (!_firstSoul)
        {
            _souls[0].transform.position = _currentSpawnLocation.position;
            _souls[0].SetActive(true);
            _firstSoul = true;
           
        }

        else if (!_secondSoul)
        {
            _souls[1].transform.position = _currentSpawnLocation.position;
            _souls[1].SetActive(true);
            _secondSoul = true;
        }
        
        else if (!_thirdSoul)
        {
            _souls[2].transform.position = _currentSpawnLocation.position;
            _souls[2].SetActive(true);
            _thirdSoul = true;
        }
    }
}
