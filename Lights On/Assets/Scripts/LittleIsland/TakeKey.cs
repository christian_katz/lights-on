﻿using System;
using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;
using UnityEngine;

public class TakeKey : MonoBehaviour
{

    [SerializeField] private string _playerTag;
    
    private bool _showButton = true;
    private PlayerUI _playerUI;
    private OpenGate _openGate;
    [SerializeField] private HealEnvironment _healEnvironment;
    
    void Start()
    {
        _openGate = FindObjectOfType<OpenGate>();
        _playerUI = FindObjectOfType<PlayerUI>();
    }
    
    void Update()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y + Mathf.Sin(Time.time) * 0.005f, transform.position.z);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag(_playerTag))
        { 
            _playerUI.ActivateHealingButton.SetActive(true);
            
            if (Input.GetKeyDown(KeyCode.F))
            {
                gameObject.SetActive(false);
                _playerUI.ActivateHealingButton.SetActive(false);
                MasterAudio.PlaySound("CollectKey");
                _openGate.CollectedKeys++;
                _healEnvironment.ChangeEnvironment();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(_playerTag))
        {
            _playerUI.ActivateHealingButton.SetActive(false);
            
        }
    }
}
