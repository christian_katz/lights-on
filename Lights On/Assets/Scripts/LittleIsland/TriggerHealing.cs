using System;
using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;
using UnityEngine;

public class TriggerHealing : MonoBehaviour
{

    [SerializeField] private string _playerTag;
    [SerializeField] private HealEnvironment _healEnvironment;

    private bool _interacted = false;

    private bool _activateHealing = false;
    
    private PlayerUI _playerUI;
    private Player _player;
    
    void Start()
    {
        _playerUI = FindObjectOfType<PlayerUI>();
        _player = FindObjectOfType<Player>();
        _playerUI.ActivateHealingButton.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F) && !_interacted && _activateHealing)
        {
            gameObject.SetActive(false);
            _playerUI.ActivateHealingButton.SetActive(false);
            MasterAudio.PlaySound("CollectKey");
            _player.PlayerAnimation.Heal(true);
            _healEnvironment.ChangeEnvironment();
            _interacted = true;

            MasterAudio.PlaySound("StartHealEnvironment");
            MasterAudio.PlaySound("HealEnvironment");
            MasterAudio.PlaySound("HealEnvironment2");
            MasterAudio.PlaySound("HealEnvironment3");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(_playerTag))
        {
            _playerUI.ActivateHealingButton.SetActive(true);
            _activateHealing = true;
        }
    }
    
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(_playerTag))
        {
            _playerUI.ActivateHealingButton.SetActive(false);
        }
    }
}
