using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerWarning : MonoBehaviour
{
    [SerializeField] private ChangeEmission[] _changeEmissions;
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            for (int i = 0; i < _changeEmissions.Length; i++)
            {
                _changeEmissions[i].EnemyWarning = true;
            }
            
            gameObject.SetActive(false);
        }
    }
}
