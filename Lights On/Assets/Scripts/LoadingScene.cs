﻿using System;
using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.UI.ProceduralImage;

public class LoadingScene : MonoBehaviour
{

    [SerializeField] private TextMeshProUGUI _loadingText;
    [SerializeField] private Image _loadingBar;
    [SerializeField] private float _loadingDuration;
    [SerializeField] private TextMeshProUGUI _continueButtonText;
    [SerializeField] private Animator[] _animator;
    [SerializeField] private Image _fadeScreenImage;

    private bool _loading = false;

    private Button _button;
    
    void Start()
    {
        _fadeScreenImage.raycastTarget = false;
        MasterAudio.ChangePlaylistByName("PlaylistController","Loading");
        _fadeScreenImage.DOFade(0, 3);
        
        for (int i = 0; i < _animator.Length; i++)
        {
            _animator[i].enabled = false;
        }

        _button = _continueButtonText.GetComponentInParent<Button>();
        _button.transform.gameObject.SetActive(false);
        _loadingBar.fillAmount = 0;
    }
    
    void Update()
    {
        if (_fadeScreenImage.color.a <= 0.1f && !_loading)
        {
            _loading = true;
            _loadingBar.DOFillAmount(1, _loadingDuration);
        }

        if (_loading)
        {
            _loadingText.text = String.Format("{00:0}" + " %" ,_loadingBar.fillAmount * 100);
            
            if (_loadingBar.fillAmount >= 0.99f)
            {
                _loadingBar.fillAmount = 1;
                _button.transform.gameObject.SetActive(true);
                _continueButtonText.DOFade(1, 3);
            }
        
            if (_fadeScreenImage.color.a >= 0.99f)
            {
                _loading = false;
                SceneManager.LoadScene("Play");
                
                MasterAudio.ChangePlaylistByName("Playing");
                MasterAudio.PlaySound("Forest");
                MasterAudio.PlaySound("Wind");
                MasterAudio.PlaySound("TreeRustle");
            }
        }

    }

    public void LoadMainScene()
    {
        for (int i = 0; i < _animator.Length; i++)
        {
            _animator[i].enabled = true;
        }

        MasterAudio.PlaySound("OpenStoneDoor");
        MasterAudio.PlaySound("OpenGateMagic");
        _fadeScreenImage.raycastTarget = true;
        _fadeScreenImage.DOFade(1, 7);
    }
}
