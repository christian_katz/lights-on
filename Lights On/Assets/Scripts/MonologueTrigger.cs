using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions.Must;

public class MonologueTrigger : MonoBehaviour
{
    private Player _player;
    
    [SerializeField] private GameObject  _conversationWindow;
    [SerializeField] private GameObject[] _text;

    [Tooltip("if there is just one text, set value to false")]
    [SerializeField] public bool _continueConversation = true;

    private int _textCounter = 0;

    [Header("Only for Coat Emission Warning")]
    [SerializeField] private bool _useWarning;
    [SerializeField] private GameObject[] _warnings;

    private bool _talk = false;
    
    void Start()
    {
        _player = FindObjectOfType<Player>();
    }

    private void Update()
    {
        if (_talk)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                if (_textCounter < _text.Length - 1)
                {
                    _text[_textCounter].SetActive(false);
                    
                    _textCounter++;
                    
                    _text[_textCounter].SetActive(true);

                    _continueConversation = false;
                    return;
                }
                
                if (!_continueConversation)
                {
                    _player.enabled = true;
                    _conversationWindow.SetActive(false);
                    _text[_textCounter].SetActive(false);
                    _textCounter = 0;
                    _continueConversation = true;

                    if (_useWarning)
                    {
                        for (int i = 0; i < _warnings.Length; i++)
                        {
                            _warnings[i].SetActive(false);
                        }
                    }

                    _talk = false;
                    gameObject.SetActive(false);
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _conversationWindow.SetActive(true);
            _text[0].SetActive(true);
            _player.enabled = false;
            _player.PlayerAnimation.PlayerSpeed(0);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _talk = true;
        }
    }
}
