﻿using System;
using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OpenGate : MonoBehaviour
{
    private int _placedKeys;
    public int PlacedKeys => _placedKeys;
    [SerializeField] private ParticleSystem _whiteLight;

    [SerializeField] private Animator[] _animator;
    [SerializeField] private Image _fadeImage;

    //prevent that the player go to the end scene when he wants to restart the game
    private bool _openGate = false;

    private Player _player;
    
    private int _collectedKeys;

    public int CollectedKeys
    {
        get => _collectedKeys;
        set => _collectedKeys = value;
    }

    private void Start()
    {
       // _player = FindObjectOfType<Player>();
       // _player.enabled = true;

        // _placedKeys = 3;
        // KeyPlaced();
    }

    private void Update()
    {
        if (_fadeImage.color.a >= 1 && _openGate)
        {
            SceneManager.LoadScene("EndScene");
            _openGate = false;
        }
    }

    public void KeyPlaced()
    {
        _placedKeys++;
        
        if (_placedKeys >= 3)
        {
            MasterAudio.PlaySound("OpenStoneDoor");
            MasterAudio.PlaySound("OpenGateMagic");
            _whiteLight.gameObject.SetActive(true);
            _fadeImage.DOFade(1, 4);
            _whiteLight.Play();
            _player.enabled = false;
            _openGate = true;
            for (int i = 0; i < _animator.Length; i++)
            {
                _animator[i].enabled = true;
            }
        }
    }
}
