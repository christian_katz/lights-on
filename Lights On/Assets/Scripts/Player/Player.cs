﻿using System;
using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;
using Cursor = UnityEngine.Cursor;

public class Player : MonoBehaviour
{

    private IPlayerState _currentState;
    public static IdleState IdleState = new IdleState();
    public static MoveState MoveState = new MoveState();
    public static AttackComboState AttackComboState = new AttackComboState();
    public static RollState RollState = new RollState();
    
    [Header("Movement")]
   [SerializeField] private float _movementSpeed;
   [SerializeField] private float _rotationSpeed;
   [SerializeField] private float _smoothMovement;
   [SerializeField] private float _smoothRotation;
   [SerializeField] private float _gravityForce;
   [SerializeField] private float _dashForce;
   
   [Header("Ground Check")]
   [SerializeField] private float _groundCheckRadius;
   [SerializeField] private LayerMask _layerMask;
   [SerializeField] private Transform _groundCheckPosition;

   [Header("Stats")] 
   [SerializeField] private float _damageValue;
   [SerializeField] private float _health;

   [Header("Focus Enemy at Attack")] 
   [SerializeField] private float _forceToEnemy = 100;
   [SerializeField] private float _minRangeFocus = 10;
   [SerializeField] private float _maxRangeFocus = 20;
   
   [Header("Attack Enemy")]
   [SerializeField] private Collider _swordCollider;
   [SerializeField] private float _stopAnimatorTimeAtHit = 0.2f;

   // Prevent, that the player gets damage despite he is dead, thus the death method will be called more than just one time. These cause problems
   private bool _dead = false;

   public float DamageValue => _damageValue;

   private CharacterController _characterController;

   public CharacterController CharacterController
   {
       get => _characterController;
       set => _characterController = value;
   }

   private PlayerAnimationController _playerAnimation;

   public PlayerAnimationController PlayerAnimation => _playerAnimation;

   private PlayerUI _playerUI;
   private SwordDissolveControl _swordDissolveControl;

   public SwordDissolveControl SwordDissolveControl
   {
       get => _swordDissolveControl;
       set => _swordDissolveControl = value;
   }

   private Camera _playerCamera;
   private float _refVelocity;
   private float _currentForwardSpeed;
   private bool _grounded;
   private float _gravity;
   private Vector3 _moveDirection;

   public float Gravity
   {
       get => _gravity;
       set => _gravity = value;
   }

   public Vector3 MoveDirection => _moveDirection;

   private bool _isDashing;
   
   public delegate void DoDamage();
   public static event DoDamage OnDamage;

   private Shader shader;
   
   private bool _enemyLock = false;
   
   private float _stopAnimatorCooldown = 0.1f;
   
   private ImpactReceiver _impactReceiver;
   
   public float CurrentForwardSpeed
   {
       get => _currentForwardSpeed;
       set => _currentForwardSpeed = value;
   }

   private bool _stopAnimator = true;
   private bool _activateDamage = false;
   private float _damageCooldown = 0.1f;

   private FightZone _currentFightZone;
   private InGameMenu _inGameMenu;

   [SerializeField] private ParticleSystem _getHitEffect;
   
   void Start()
    {
        _characterController = GetComponent<CharacterController>();
        _playerAnimation = GetComponent<PlayerAnimationController>();
        _playerUI = GetComponent<PlayerUI>();
        _swordDissolveControl = GetComponent<SwordDissolveControl>();
        _impactReceiver = GetComponent<ImpactReceiver>();
        _inGameMenu = FindObjectOfType<InGameMenu>();

        _currentState = IdleState;
        
        _swordCollider.enabled = false;

        _stopAnimatorCooldown = _stopAnimatorTimeAtHit;
        
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        
        MasterAudio.ChangePlaylistByName("PlaylistController","Playing");
        
    }
    
    void Update()
    {
        IPlayerState nextState = _currentState.Execute(this);
        if (nextState != _currentState)
        {
            _currentState.Exit(this);
            _currentState = nextState;
            _currentState.Enter(this);
        }
        
        DeactivateSwordCooldown();
    }

    public void MovementExecution()
    {
        GroundCheck();
        MovementDirection();
        PlayerRotation(); 
        Movement();
    }
    
    private void Movement()
    {
        _currentForwardSpeed = Mathf.SmoothDamp(_currentForwardSpeed, _moveDirection.normalized.magnitude * _movementSpeed, ref _refVelocity, Time.deltaTime * _smoothMovement);
        _characterController.Move(_moveDirection * (_currentForwardSpeed * Time.deltaTime) + new Vector3(0,_gravity,0) * Time.deltaTime);
        _playerAnimation.PlayerSpeed(_moveDirection.magnitude);
    }

    public void MovementDirection()
    {
        float horizontalAxis = Input.GetAxis("Horizontal");
        float verticalAxis = Input.GetAxis("Vertical");
        
        _moveDirection = Camera.main.transform.forward * verticalAxis + Camera.main.transform.right * horizontalAxis;
        _moveDirection.y = 0;
    }
    
    public void PlayerRotation()
    {
        if (_moveDirection.magnitude >= 0.1f)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(_moveDirection * _rotationSpeed),  Time.deltaTime * _smoothRotation);
        }
    }
    
    public void GroundCheck()
    {
        _grounded = Physics.CheckSphere(_groundCheckPosition.position, _groundCheckRadius, _layerMask);

        if (_grounded)
        {
            _gravity = 0;
        }
        else
        {
            _gravity += Physics.gravity.y * Time.deltaTime * _gravityForce;
        }
    }

    public void Rolling()
    {
        _impactReceiver.AddImpact(transform.forward, 200);
    }
    
    public void GetDamage()
    {
        _health -= 10f;
        _playerUI.HealthBar.fillAmount -= 10f / 300;
        MasterAudio.PlaySound("PlayerGetHit");
        _getHitEffect.Play();
        
        if (_health <= 0 && !_dead)
        {
            _playerAnimation.Dies();
            enabled = false;
            _inGameMenu.DeathScreen();
            FindObjectOfType<Enemy>().EnemyDisappear();
            _dead = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("EnemySword") && !_dead)
        {
            GetDamage();
        }
    }

    public void FaceEnemy()
    {
        if (_currentFightZone != null && _currentFightZone.isActiveAndEnabled)
        {
            GameObject enemy = _currentFightZone.Enemy;
            Vector3 _enemyDirection = (enemy.transform.position - transform.position).normalized;
            _enemyDirection.y = 0;
            transform.rotation = Quaternion.LookRotation(_enemyDirection);
        }

    }
    
    public void CurrentFightZone(FightZone fightZone)
    {
        _currentFightZone = fightZone;
    }
    
    /// <summary>
    /// Method will be activated from the animation events
    /// </summary>
    public void EnemyGetDamage()
    {
        _swordCollider.enabled = true;
        _stopAnimator = true;
       // _playerAnimation.Animator.speed = 0;
       _activateDamage = true;
    }
    
    private void AnimatorSpeedControl()
    {
        if (_stopAnimator)
        {
            _swordCollider.enabled = false;

            _stopAnimatorCooldown -= Time.deltaTime;
            if(_stopAnimatorCooldown <= 0)
            {
                _stopAnimatorCooldown = _stopAnimatorTimeAtHit;
                _stopAnimator = false;
               // _playerAnimation.Animator.speed = 1;
            }
        }
    }
    
    private void DeactivateSwordCooldown()
    {
        if (_activateDamage)
        {
            _damageCooldown -= Time.deltaTime;
            if (_damageCooldown <= 0)
            {
                _swordCollider.enabled = false;
                _damageCooldown = 0.1f;
                _activateDamage = false;
            }
        }
    }
    
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(_groundCheckPosition.position, _groundCheckRadius);
    }
}
