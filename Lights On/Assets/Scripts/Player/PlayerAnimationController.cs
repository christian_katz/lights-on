﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationController : MonoBehaviour
{
    private Animator _animator;

    public Animator Animator => _animator;
    private ImpactReceiver _impactReceiver;
    
    private static readonly int MovementSpeed = Animator.StringToHash("movementSpeed");
    
    private int _nuOfClicks = 0;
    private bool _backToIdleState = false;
    public bool BackToIdleState
    {
        get => _backToIdleState;
        set => _backToIdleState = value;
    }

    private bool _rolling = true;
    private static readonly int Die = Animator.StringToHash("die");
    private static readonly int CloseHand = Animator.StringToHash("closeHand");
    private static readonly int Healing = Animator.StringToHash("healing");
    
    public bool Rolling
    {
        get => _rolling;
        set => _rolling = value;
    }

    public int NuOfClicks
    {
        get => _nuOfClicks;
        set => _nuOfClicks = value;
    }

    void Start()
    {
        _animator = GetComponent<Animator>();
        _impactReceiver = GetComponent<ImpactReceiver>();
    }
    
    public void PlayerSpeed(float speed)
    {
        _animator.SetFloat(MovementSpeed, speed);
    }

    public void Dies()
    {
        _animator.SetTrigger(Die);
    }

    // Animation Event for all where to push forward
    public void MoveForward(float force)
    {
        _impactReceiver.AddImpact(transform.forward,force);
    }

    public void Roll(bool reset)
    {
        if (!reset)
        {
            _animator.SetTrigger("roll"); 
            _rolling = true;
        }
        else
        {
            _animator.ResetTrigger("roll");
        }
    }

    // Roll Animation Event
    public void MoveAfterRoll()
    {
        _rolling = false;
    }

    
    public void ComboCheck()
    {
        if (_animator.GetCurrentAnimatorStateInfo(0).IsName("Idle") && _nuOfClicks >= 1 || _animator.GetCurrentAnimatorStateInfo(0).IsName("AfterAttack3") || _animator.GetCurrentAnimatorStateInfo(0).IsName("AfterAttack2") || _animator.GetCurrentAnimatorStateInfo(0).IsName("AfterAttack") && _nuOfClicks >= 1 || _animator.GetCurrentAnimatorStateInfo(0).IsName("Run")  && _nuOfClicks >= 1)
        {
            _animator.SetTrigger("firstAttack");
            
            _nuOfClicks = 0;
        }
        else if (_animator.GetCurrentAnimatorStateInfo(0).IsName("Attack")  && _nuOfClicks >= 1)
        {
            _animator.ResetTrigger("firstAttack");
            _animator.SetTrigger("secondAttack");
            
            _nuOfClicks = 0;

            
        }
        else if (_animator.GetCurrentAnimatorStateInfo(0).IsName("Attack2") && _nuOfClicks >= 1)
        {
            _animator.ResetTrigger("secondAttack");
            _animator.SetTrigger("thirdAttack");
            
            _nuOfClicks = 0;
        }
    }

    // Idle & After Attack Animation Event
    public void ResetClickNumber()
    {
        _nuOfClicks = 0;
        _backToIdleState = true;
        _animator.ResetTrigger("firstAttack");
        _animator.ResetTrigger("secondAttack");
        _animator.ResetTrigger("thirdAttack");
    }

    public void WeaponHand(bool close)
    {
        _animator.SetBool(CloseHand, close);
    }

    public void Heal(bool healing)
    {
        _animator.SetBool(Healing, healing);
    }
}
