﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
    [SerializeField] private Image _healthBar;
    [SerializeField] private GameObject _activateHealingButton;
    [SerializeField] private GameObject _endGameButton;
    [SerializeField] private GameObject _talkKey;

    private int _keyCounter;

    public GameObject ActivateHealingButton => _activateHealingButton;
    public GameObject EndGameButton => _endGameButton;

    public GameObject TalkKey => _talkKey;

    public Image HealthBar => _healthBar;
    
}
