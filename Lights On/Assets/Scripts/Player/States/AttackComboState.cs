﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackComboState : IPlayerState
{
  private bool _attack => Input.GetMouseButtonDown(0);

  public IPlayerState Execute(Player player)
  {
    bool _rolling = Input.GetKeyDown(KeyCode.LeftControl);
    if (_rolling)
    {
      return Player.RollState;
    }
    
    if (_attack)
    {
      player.PlayerAnimation.NuOfClicks++;
      player.FaceEnemy();
      //player.EnemyLock = true;
    }
    player.PlayerAnimation.ComboCheck();
    
    if (player.PlayerAnimation.BackToIdleState && !_attack)
    {
      player.PlayerAnimation.BackToIdleState = false;
      return Player.IdleState;
    }
    
    return Player.AttackComboState;
  }
  
  public void Enter(Player player)
  {
    // deactivates cooldown to go back to make the sword invisible
    player.SwordDissolveControl.Dissolve = false;
    
    // give the order to be able to make the sword visible
    player.SwordDissolveControl.GOInvisible = false;

    player.SwordDissolveControl.GOVisible = true;

    // reset seconds to dissolve the sword
    player.SwordDissolveControl.Cooldown = 3;
    
    // set the speed for the run animation and current movement speed to 0 so that the character is not moving or running when attacking
    player.CurrentForwardSpeed = 0;
    player.PlayerAnimation.PlayerSpeed(0);
    
  }

  public void Exit(Player player)
  {
      // activates cooldown to go back to make the sword invisible
      player.SwordDissolveControl.Dissolve = true;
      
  }
}
