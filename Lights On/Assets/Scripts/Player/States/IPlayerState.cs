﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    public interface IPlayerState
    {
        // the Execute method is for the update of the state
        IPlayerState Execute(Player player);
        // the Enter method will be executed once when the state will be entered
        void Enter(Player player);
        // the Exit method will be executed once when the state will be exited
        void Exit(Player player);
    }

