﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class IdleState : IPlayerState
{
    private bool _attack => Input.GetMouseButtonDown(0);
    
    public IPlayerState Execute(Player player)
    {
        bool _rolling = Input.GetKeyDown(KeyCode.LeftControl);
        if (_rolling)
        {
            return Player.RollState;
        }
        
        bool _move = player.MoveDirection.magnitude >= 0.1f;
        if (_move)
        {
            return Player.MoveState;
        }

        if (_attack)
        {
            player.PlayerAnimation.NuOfClicks++;
            player.PlayerAnimation.ComboCheck();
            player.FaceEnemy();
            return Player.AttackComboState;
        }
        
        player.MovementExecution();
        return Player.IdleState;
    }

    public void Enter(Player player)
    {
        player.PlayerAnimation.NuOfClicks = 0;
    }

    public void Exit(Player player)
    {

    }
}
