﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveState :  IPlayerState
{
    private bool _attack => Input.GetMouseButtonDown(0);

    public IPlayerState Execute(Player player)
    {
        bool _rolling = Input.GetKeyDown(KeyCode.LeftControl);
        if (_rolling)
        {
            return Player.RollState;
        }
        
        if (_attack)
        {
            player.PlayerAnimation.NuOfClicks++;
            player.PlayerAnimation.ComboCheck();
            player.FaceEnemy();
            return Player.AttackComboState;
        }
        
        bool move = player.MoveDirection.magnitude >= 0.1f;
        if (!move)
        {
            return Player.IdleState;
        }
        
        player.MovementExecution();
        return Player.MoveState;
    }

    public void Enter(Player player)
    {
        player.PlayerAnimation.NuOfClicks = 0;
    }

    public void Exit(Player player)
    {

    }
}
