﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RollState : IPlayerState
{
    public IPlayerState Execute(Player player)
    {
        if (!player.PlayerAnimation.Rolling)
        {
            return Player.IdleState;
        }

        player.GroundCheck();
        player.CharacterController.Move(new Vector3(0,player.Gravity,0));
        return Player.RollState;
    }

    public void Enter(Player player)
    {
        player.PlayerAnimation.Roll(false);
        player.Rolling();
    }

    public void Exit(Player player)
    {
        player.PlayerAnimation.Roll(true);
    }
}
