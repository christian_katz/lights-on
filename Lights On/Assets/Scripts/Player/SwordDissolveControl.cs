﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordDissolveControl : MonoBehaviour
{
    private PlayerAnimationController _playerAnimationController;
    
    [SerializeField] private Material[] _dissolveMaterial;

    private float _fadeOutTime = 0;

    private float _fadeTime = 1;
    [SerializeField] private float _fadeInSpeed = 0;
    [SerializeField] private bool _goVisible = true;

    public bool GOVisible
    {
        get => _goVisible;
        set => _goVisible = value;
    }

    // the order to go invisible or not
    private bool _goInvisible = false;

    // activates the dissolve cooldown
    private bool _dissolve = false;

    public bool Dissolve
    {
        get => _dissolve;
        set => _dissolve = value;
    }

    private float _cooldown;

    public float Cooldown
    {
        get => _cooldown;
        set => _cooldown = value;
    }

    public bool GOInvisible
    {
        get => _goInvisible;
        set => _goInvisible = value;
    }

    void Start()
    {
        _playerAnimationController = GetComponent<PlayerAnimationController>();
        
        if (_goInvisible)
        {
            _fadeTime = 0;
            _goVisible = false;
        }
        else
        {
            _fadeTime = 1;
            _goVisible = true;
        }
    }

    void Update()
    {
        ObjectAppearing();
        DissolveCooldown();
    }

    private void ObjectAppearing()
    {
        // object will be visible
        if (_goVisible && !_goInvisible)
        {
            _playerAnimationController.WeaponHand(true);
            
            _fadeTime -= Time.deltaTime * _fadeInSpeed / 10;

            for (int i = 0; i < _dissolveMaterial.Length; i++)
            {
                _dissolveMaterial[i].SetFloat("Vector1_9C8F1CA7", _fadeTime);

                if (_dissolveMaterial[i].GetFloat("Vector1_9C8F1CA7") <= 0)
                {
                    _goVisible = false;
                }
            }
        }

        // object will be invisible
        if (!_goVisible && _goInvisible)
        {
            _playerAnimationController.WeaponHand(false);
            
            _fadeTime += Time.deltaTime * _fadeInSpeed / 10;

            for (int i = 0; i < _dissolveMaterial.Length; i++)
            {
                _dissolveMaterial[i].SetFloat("Vector1_9C8F1CA7", _fadeTime);

                if (_dissolveMaterial[i].GetFloat("Vector1_9C8F1CA7") >= 1)
                {
                    _goVisible = true;
                }
            }
        }
    }

    private void DissolveCooldown()
    {
        if (_dissolve)
        {
            _cooldown -= Time.deltaTime;
            if (_cooldown <= 0 || !_dissolve)
            {
                _cooldown = 3;
                _goInvisible = true;
            }
        }
    }
}
