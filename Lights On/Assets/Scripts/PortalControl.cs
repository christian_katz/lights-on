using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalControl : MonoBehaviour
{
    private Renderer _renderer;
    private Player _player;
    private float _dissolveNumber;
    
    // Start is called before the first frame update
    void Start()
    {
        _player = FindObjectOfType<Player>();
        _renderer = GetComponent<Renderer>();
        _dissolveNumber = _renderer.material.GetFloat("Vector1_520a94eabbb8417aabd36f733c2bd5cd");
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(transform.position, _player.transform.position) < 30)
        {
            _dissolveNumber = Mathf.Lerp(_dissolveNumber, -1.54f, Time.deltaTime * 6);
            _renderer.material.SetFloat("Vector1_520a94eabbb8417aabd36f733c2bd5cd", _dissolveNumber);
        }
        
        if (Vector3.Distance(transform.position, _player.transform.position) > 30)
        {
            _dissolveNumber = Mathf.Lerp(_dissolveNumber, 10, Time.deltaTime * 1);
            _renderer.material.SetFloat("Vector1_520a94eabbb8417aabd36f733c2bd5cd", _dissolveNumber);
        }
    }
}
