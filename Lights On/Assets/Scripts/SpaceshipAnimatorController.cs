using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceshipAnimatorController : MonoBehaviour
{
   [SerializeField] private float _vanishCooldown = 30;
    
    void Update()
    {
        _vanishCooldown -= Time.deltaTime;
    }

    public void StopSpaceship()
    {
        if (_vanishCooldown <= 0)
        {
            gameObject.SetActive(false);
        }
    }
}
