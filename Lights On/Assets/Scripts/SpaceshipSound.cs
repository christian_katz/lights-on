using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;
using UnityEngine;

public class SpaceshipSound : MonoBehaviour
{
    public void PlaySpaceshipSound()
    {
        MasterAudio.PlaySound("Spaceship");
    }
}
