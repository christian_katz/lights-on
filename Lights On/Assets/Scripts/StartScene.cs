using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartScene : MonoBehaviour
{
    [SerializeField] private Image _fadeImage;

    private bool _fadeIn = false;

    public Image FadeImage
    {
        get => _fadeImage;
        set => _fadeImage = value;
    }

    private DialogueTriggerStartScene _dialogueTrigger;
    private DialogueManagerStartScene _dialogueManager;
    
    void Start()
    {
        _dialogueManager = GetComponent<DialogueManagerStartScene>();
        _dialogueTrigger = GetComponent<DialogueTriggerStartScene>();
        
        _fadeImage.DOFade(0, 3);
        
        _dialogueTrigger.TriggerDialogue();

    }
    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            _dialogueManager.DisplayNextSentence();
        }
        
        if (_fadeImage.color.a <= 0.9f)
        {
            _fadeIn = true;
        }

        if (_fadeImage.color.a >= 1 && _fadeIn)
        {
            SceneManager.LoadScene("Loading");
        }
    }
}
