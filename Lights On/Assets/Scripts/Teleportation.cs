using System;
using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;
using UnityEngine;

public class Teleportation : MonoBehaviour
{
    [SerializeField] private GameObject _player;
    [SerializeField] private Transform _teleportationLocation;
    
    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            MasterAudio.PlaySound("TeleportationPlayer");
            _player.GetComponent<Player>().enabled = false;
            _player.GetComponent<CharacterController>().enabled = false;
            _player.transform.position = _teleportationLocation.position;
            _player.GetComponent<CharacterController>().enabled = true;
            _player.GetComponent<Player>().enabled = true;
        }

    }
}
